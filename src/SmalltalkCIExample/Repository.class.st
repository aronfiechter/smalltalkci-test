"
I represent a git Repository with a repoDirectory and a list of Commits.
"
Class {
	#name : #Repository,
	#superclass : #Object,
	#instVars : [
		'repoDirectory',
		'commits',
		'url',
		'name',
		'lGitRepository'
	],
	#classInstVars : [
		'repositoriesRoot'
	],
	#category : #SmalltalkCIExample
}

{ #category : #'class initialization' }
Repository class >> defaultRepositoriesRoot [
	^ '/var/tmp/repositories/' asFileReference
]

{ #category : #'class initialization' }
Repository class >> fromGitHubUrl: aString [ 
	| repo |
	repo := self new
		url: aString;
		extractName;
		yourself.
	^ repo
]

{ #category : #'class initialization' }
Repository class >> initialize [
	repositoriesRoot := self defaultRepositoriesRoot
]

{ #category : #accessing }
Repository class >> repositoriesRoot [
	^ repositoriesRoot
]

{ #category : #accessing }
Repository >> commits [
	^ commits ifNil: [ commits := self loadCommits ]
]

{ #category : #accessing }
Repository >> commits: anObject [
	commits := anObject
]

{ #category : #actions }
Repository >> extractName [
	name := ('-' join: (self url asZnUrl segments))
		asFileReference withoutExtension path base
]

{ #category : #initialization }
Repository >> initializeLGitRepository [
	lGitRepository := LGitRepository on: self repoDir.
	[  lGitRepository clone: self url ] on: LGit_GIT_EEXISTS do: [ :e |
		lGitRepository open.
		lGitRepository checkout: 'master'.
	]
]

{ #category : #accessing }
Repository >> lGitRepository [
	lGitRepository ifNil: [ self initializeLGitRepository ].
	lGitRepository isInitialized ifFalse: [ self initializeLGitRepository ].
	^ lGitRepository
]

{ #category : #actions }
Repository >> loadCommits [
	| revWalk lGitCommits |
	self lGitRepository isInitialized ifFalse: [ self initializeLGitRepository ].
	revWalk := LGitRevwalk of: self lGitRepository.
	lGitCommits := OrderedCollection new.
	revWalk pushReference: self lGitRepository head.
	revWalk do: [ :c | lGitCommits add: c ].
	^ lGitCommits collect: [ :each |
		Commit fromLGitCommit: each onRepository: self.
	]
]

{ #category : #accessing }
Repository >> name [
	^ name
]

{ #category : #'accessing-computed' }
Repository >> repoDir [
	^ self class repositoriesRoot / self name.
]

{ #category : #accessing }
Repository >> repoDirectory [
	^ repoDirectory
]

{ #category : #accessing }
Repository >> repoDirectory: anObject [
	repoDirectory := anObject
]

{ #category : #accessing }
Repository >> url [
	^ url
]

{ #category : #accessing }
Repository >> url: aString [ 
	url := aString
]
